const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const webpack = require('webpack'); //to access built-in plugins
const path = require('path');
const TerserPlugin = require("terser-webpack-plugin");

module.exports = {
  entry: {
    app: './app/index.tsx', // TODO: look into multiple entry. Think required for MPA.
  }, 
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'build'),
    clean: true,
  },
  resolve: {
    extensions: [".js", ".json", ".ts", ".tsx"],
  },
  // for dev only
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './build', // webpack-dev-server serve files from build on localhost:8080
    hot: true,
  },
  module: {
    rules: [
      // css  yarn add style-loader css-loader --dev
      {
        test: /\.css$/,
        // these are executed in reverse order
        use: ['style-loader', 'css-loader'],
      },
      // typescript. yarn add ts-loader --dev
      {
        test: /\.(ts|tsx)$/,
        use: 'ts-loader',
      },
      // graphic assets
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },
      // font assets
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: 'asset/resource',
      },
      // csv, xml, (json is loaded by default already),
      {
        test: /\.(csv|tsv)$/i,
        use: ['csv-loader'],
      },
      {
        test: /\.xml$/i,
        use: ['xml-loader'],
      },
      // babel
      {
        test: /\.js$/,
        include: path.resolve(__dirname, 'src'),
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env', '@babel/preset-react'],
          plugins: [],
        }
      },
    ],
  },
  plugins: [new HtmlWebpackPlugin({ template: './public/index.html' })],
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()],
  },
  mode: 'production',
  target: 'web',
};
