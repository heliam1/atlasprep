import React, { useState } from 'react';

//export interface FolderProps {
//  name: string,
//  children?: Array<String | typeof Folder>,
//}

export default function Folder(props: any): JSX.Element {
  const [open, setOpen] = useState(true);

  // only display children if open
  // O(n) for children. Filter for folders through children first pass, filter for filenames for children second pass.

  return (<>
    {
      open
      ? <p onClick={() => { setOpen(!open); }}>o</p> 
      : <p onClick={() => { setOpen(!open); }}>-</p>
    }
    <p
      onClick={() => { setOpen(!open); }}
    >
      {props.name}
    </p>
    {
      open
      && props.children !== undefined
      && props.children
      .filter((item: any) => {
        return item instanceof Folder;
      })
    }
    {
      open
      && props.children !== undefined
      && props.children
      .filter((item: any) => {
        return !(item instanceof Folder);
      })
    }
  </>);
}
