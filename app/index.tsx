import React from 'react';
import { render } from 'react-dom';
import Folder from './ui/folder';
// import { Provider } from 'react-redux';
// import { History } from 'history';
// import { Store } from 'store';
// import Routes from 'Routes';

// Use DOMContentLoaded rather than window.onload since onload waits for 
// all resources including images to be loaded
document.addEventListener('DOMContentLoaded', () => {
  // TODO: CodeSplitting
  // const Root = require('./containers/Root').default;

  render(
    <>
      <p>Atlas Prep</p>
      <Folder name={"myFolder1"}>
        <Folder name={"myFolder2"}>
          <p>A file name3</p>
        </Folder>
        <p>A file name1</p>
        <p>A file name2</p>
      </Folder>
    </>,
    document.getElementById('root')
  );
});

/*
css box, container, flexbox, 

creating recursive react component
  that doesn't have bugs
padding/inline style/styled component
  row/column, layout
element, conditional rendering

icon/image

add statemanagement, nav, react component that utilize ds/a.
services

read the cv that was sent to atlas

implement React.memo, useCallback, useMemo

then gg ez

need to drill deeper into webpack, css

webp
  webpack babel ts
  babel ts
  tsconfig noemit true

  looks like sourcemap is created in app = bad

  TODO %publicurl%

  code splitting/lazy

  prodvsdev

css
  inline vs styledcomponent vs sass vs postcss
*/